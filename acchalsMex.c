#include <stdio.h>
#include <mex.h>
#include <math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <stdlib.h>
#include <time.h>

//////////////////////////////////////////////////////////////////////////////
///// Utils //////////////////////////////////////////////////////////////////

/* Convert a mxArray to a gsl_matrix */
gsl_matrix *
mxmat_to_gslmat (const mxArray * mxmat)
{
  const mwSize *dims = mxGetDimensions (mxmat);
  int nb_row = (int) dims[0];
  int nb_col = (int) dims[1];
  double *rawmat = mxGetPr (mxmat);

  gsl_matrix *m = gsl_matrix_alloc (nb_row, nb_col);
  for (int i = 0; i < nb_row; ++i)
    for (int j = 0; j < nb_col; ++j)
      gsl_matrix_set (m, i, j, rawmat[i + j * nb_row]);

  return m;
}


/* Convert a gsl_matrix to a mxArray */
mxArray *
gslmat_to_mxmat (const gsl_matrix * gslmat)
{
  int nb_row = gslmat->size1;
  int nb_col = gslmat->size2;

  mxArray *m = mxCreateDoubleMatrix (nb_row, nb_col, mxREAL);
  double *pm = mxGetPr (m);
  for (int i = 0; i < nb_row; ++i)
    for (int j = 0; j < nb_col; ++j)
      pm[i + j * nb_row] = gsl_matrix_get (gslmat, i, j);

  return m;
}


/* Return the sum of all elements of the given matrix */
double
sum_elements_gslmat (gsl_matrix * m)
{
  double sum = 0.0;
  for (int i = 0; i < m->size1; ++i)
    for (int j = 0; j < m->size2; ++j)
      sum += gsl_matrix_get (m, i, j);
  return sum;
}


/* Set to epsilon all negative components of the given vector  */
void
nonnegativizer_gslvect (gsl_vector * v, double epsilon)
{
  for (int i = 0; i < v->size; ++i)
    if (gsl_vector_get (v, i) <= 0)
      gsl_vector_set (v, i, epsilon);
}


//////////////////////////////////////////////////////////////////////////////
///// Calculations ///////////////////////////////////////////////////////////

/* Return the Frobenius norm of the given matrix */
double
frobenius_norm (gsl_matrix * m)
{
  double norm = 0.0;
  for (int i = 0; i < m->size1; ++i)
    for (int j = 0; j < m->size2; ++j)
      {
	double value = gsl_matrix_get (m, i, j);
	norm += value * value;
      }
  return sqrt (norm);
}


/* Return ||M-UV||_F */
double
compute_error (gsl_matrix * m, double norm_m, gsl_matrix * v,
	       gsl_matrix * a_v, gsl_matrix * b_v)
{
  gsl_matrix *tmp1 = gsl_matrix_calloc (v->size1, v->size2);	/* V.*A */
  gsl_matrix *tmp2 = gsl_matrix_calloc (v->size1, v->size1);	/* B.*(V*V') */

  gsl_matrix_memcpy (tmp1, a_v);
  gsl_matrix_mul_elements (tmp1, v);

  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, v, v, 0.0, tmp2);
  gsl_matrix_mul_elements (tmp2, b_v);

  double err = sqrt (norm_m - 2 * sum_elements_gslmat (tmp1)
		     + sum_elements_gslmat (tmp2));

  gsl_matrix_free (tmp1);
  gsl_matrix_free (tmp2);
  return err;
}


/* Scale the matrix U for better initial error */
void
scaling (gsl_matrix * u, gsl_matrix * v, gsl_matrix * m)
{
  /* Init temporary matrices */
  double scaling;
  gsl_matrix *a = gsl_matrix_calloc (m->size1, v->size1);
  gsl_matrix *b = gsl_matrix_calloc (v->size1, v->size1);
  gsl_matrix *utu = gsl_matrix_calloc (v->size1, v->size1);

  /* Compute A = M.V' and B = V.V' and UtU = U'U */
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, m, v, 0.0, a);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, v, v, 0.0, b);
  gsl_blas_dgemm (CblasTrans, CblasNoTrans, 1.0, u, u, 0.0, utu);

  /* Compute A.*U and B.*(U'U) */
  gsl_matrix_mul_elements (a, u);
  gsl_matrix_mul_elements (b, utu);

  /* Scale U */
  scaling = sum_elements_gslmat (a) / sum_elements_gslmat (b);
  gsl_matrix_scale (u, scaling);

  /* Clean memory */
  gsl_matrix_free (a);
  gsl_matrix_free (b);
  gsl_matrix_free (utu);
  return;
}


//////////////////////////////////////////////////////////////////////////////
///// HALS update ////////////////////////////////////////////////////////////
/* Update of V with HALS  */
/* It can be used to update U if called with U' and relevant A, B, C */
void
hals_updt (gsl_matrix * v, gsl_matrix * a, gsl_matrix * b, gsl_vector * delta)
{
  /* Iterate on all rows of V */
  int rank = v->size1;
  for (int k = 0; k < rank; ++k)
    {
      gsl_vector_view v_row_k = gsl_matrix_row (v, k);
      gsl_vector_view a_row_k = gsl_matrix_row (a, k);
      gsl_vector_view b_row_k = gsl_matrix_row (b, k);

      /* Computation of delta = (Ak-Bk*V)/Bkk */
      gsl_blas_dgemv (CblasTrans, -1.0, v, &b_row_k.vector, 0.0, delta);
      gsl_blas_daxpy (1.0, &a_row_k.vector, delta);
      double bkk = gsl_matrix_get (b, k, k);
      gsl_vector_scale (delta, 1 / bkk);
      gsl_blas_daxpy (1.0, delta, &v_row_k.vector);
      /* Nonnegativity and safety againt div by 0 */
      nonnegativizer_gslvect (&v_row_k.vector, 1.0e-16);
    }
  return;
}


//////////////////////////////////////////////////////////////////////////////
///// Main loop: two-blocks coordinate descent ///////////////////////////////
void
mainloop (gsl_matrix * mat, double norm_m, gsl_matrix * u, gsl_matrix * v,
	  int iterlimit, double timelimit, gsl_matrix * error_time,
	  double alpha)
{
  /* Find dimensions and rank */
  int dim_m = mat->size1;
  int dim_n = mat->size2;
  int rank = u->size2;

  /* Init transposes of matrices */
  gsl_matrix *mat_tr = gsl_matrix_calloc (dim_n, dim_m);
  gsl_matrix *u_tr = gsl_matrix_calloc (rank, dim_m);
  gsl_matrix_transpose_memcpy (mat_tr, mat);
  gsl_matrix_transpose_memcpy (u_tr, u);

  /* Init intermediary matrices */
  gsl_matrix *a_u_tr = gsl_matrix_calloc (rank, dim_m);	/* V.M' */
  gsl_matrix *b_u_tr = gsl_matrix_calloc (rank, rank);	/* V.V' */
  gsl_vector *delta_u = gsl_vector_calloc (dim_m);
  gsl_matrix *a_v = gsl_matrix_calloc (rank, dim_n);	/* U'.M */
  gsl_matrix *b_v = gsl_matrix_calloc (rank, rank);	/* U'.U */
  gsl_vector *delta_v = gsl_vector_calloc (dim_n);

  /* Init time counter. tic = instant and time = duration */
  double cur_time = 0.0;
  double err;
  clock_t start_tic;
  clock_t comp_time;
  clock_t updt_tic;
  clock_t v_tic;

  /* Optimize alternatively U and V */
  for (int i = 0; i < iterlimit; ++i)
    {
      start_tic = clock ();

      /* Compute Au' = V.M' and Bu' = V.V' */
      gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, v, mat, 0.0, a_u_tr);
      gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, v, v, 0.0, b_u_tr);
      comp_time = clock () - start_tic;
      /* Update U' */
      updt_tic = clock ();
      while ((clock () - updt_tic) < alpha * comp_time)
	hals_updt (u_tr, a_u_tr, b_u_tr, delta_u);
      gsl_matrix_transpose_memcpy (u, u_tr);

      /* Compute Av = U'.M and Bv = U'.U */
      v_tic = clock ();
      gsl_blas_dgemm (CblasTrans, CblasNoTrans, 1.0, u, mat, 0.0, a_v);
      gsl_blas_dgemm (CblasTrans, CblasNoTrans, 1.0, u, u, 0.0, b_v);
      comp_time = clock () - v_tic;
      /* Update V */
      updt_tic = clock ();
      while ((clock () - updt_tic) < alpha * comp_time)
	hals_updt (v, a_v, b_v, delta_v);

      /* End loop if above timelimit */
      cur_time += ((double) (clock () - start_tic) / CLOCKS_PER_SEC);
      if (cur_time > timelimit)
	break;

      /* Compute error */
      err = compute_error (mat, norm_m, v, a_v, b_v);
      gsl_matrix_set (error_time, 0, i, cur_time);
      gsl_matrix_set (error_time, 1, i, err);
    }

  /* Clean memory */
  gsl_matrix_free (mat_tr);
  gsl_matrix_free (u_tr);
  gsl_matrix_free (a_u_tr);
  gsl_matrix_free (b_u_tr);
  gsl_vector_free (delta_u);
  gsl_matrix_free (a_v);
  gsl_matrix_free (b_v);
  gsl_vector_free (delta_v);
  return;
}


//////////////////////////////////////////////////////////////////////////////
///// Interface with Matlab: MEX function ////////////////////////////////////
void
mexFunction (int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[])
{
  /* Convert input to gsl_matrix */
  gsl_matrix *in_m = mxmat_to_gslmat (prhs[0]);
  gsl_matrix *u = mxmat_to_gslmat (prhs[1]);
  gsl_matrix *v = mxmat_to_gslmat (prhs[2]);
  double norm_m = pow (frobenius_norm (in_m), 2);

  /* Get parameters */
  double *params = mxGetPr (prhs[3]);
  int iterlimit = (int) params[0];
  double timelimit = params[1];
  double alpha = params[2];

  /* Initialize outputs */
  gsl_matrix *error_time = gsl_matrix_calloc (2, iterlimit);

  /* Scaling of U */
  scaling (u, v, in_m);

  /* Main loop */
  mainloop (in_m, norm_m, u, v, iterlimit, timelimit, error_time, alpha);

  /* Better error_time output */
  int nbiter = 0;
  for (int i = 0; i < error_time->size2; ++i)
    {
      if (gsl_matrix_get (error_time, 0, i) == 0)
	{
	  nbiter = i;
	  break;
	}
    }
  mxArray *error = mxCreateDoubleMatrix (1, nbiter, mxREAL);
  mxArray *time = mxCreateDoubleMatrix (1, nbiter, mxREAL);
  double *pr_error = mxGetPr (error);
  double *pr_time = mxGetPr (time);
  for (int i = 0; i < nbiter; ++i)
    {
      pr_time[i] = gsl_matrix_get (error_time, 0, i);
      pr_error[i] = gsl_matrix_get (error_time, 1, i);
    }

  /* Convert outputs and prepare the return */
  plhs[0] = gslmat_to_mxmat (u);
  plhs[1] = gslmat_to_mxmat (v);
  plhs[2] = error;
  plhs[3] = time;

  /* Clean memory */
  gsl_matrix_free (in_m);
  gsl_matrix_free (u);
  gsl_matrix_free (v);
  gsl_matrix_free (error_time);
  return;
}
