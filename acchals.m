% command to compile mex file:
% mex acchalsMex.c -lgsl -lgslcblas

% input matrix
M = rand(2000,2000);
[m,n] = size(M);
r = 50;
U0 = rand(m,r);
V0 = rand(r,n);

% M = [0.1 0.1 0.1 ; 0.1 0.1 0.1 ; 0.1 0.1 0.1];
% U0 = [0.2 0.2 ; 0.2 0.2 ; 0.2 0.2];
% V0 = [0.2 0.2 0.2 ; 0.2 0.2 0.2];

% parameters:  iterlimit, timelimit, alpha (acceleration param)
params = [100, 5, 0.5];

% disp(M);

[U, V, err, time] = acchalsMex(M, U0, V0, params);

% disp(U);
% disp(V);

disp(err);
disp(time);

norm((M-U*V),'fro')^2;
