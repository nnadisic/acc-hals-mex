% Ancienne version 'optimisée'
deltaV = max( (UtM(k,:)-UtU(k,:)*V)/UtU(k,k) , -V(k,:) );
V(k,:) = V(k,:) + deltaV;
nodelta = nodelta + deltaV*deltaV'; % used to compute norm(V0-V,'fro')^2;

% version 'simplifiée'
voldk = V(k,:);
V(k,:) = max( (UtM(k,:)-UtU(k,:)*V)/UtU(k,k) - V(k,:)  , 0 );
nodelta = nodelta + norm(voldk-V(k,:))^2; % used to compute norm(V0-V,'fro')^2;

% version encore 'simplifiée'
voldk = V(k,:);
K = [1:k-1 k+1:r];
V(k,:) = max( (UtM(k,:)-UtU(k,K)*V(K,:))/UtU(k,k)  , 0 );
nodelta = nodelta + norm(voldk-V(k,:))^2; % used to compute norm(V0-V,'fro')^2;
